package com.epam.model;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MinesweeperTest {
    private Minesweeper minesweeper = new Minesweeper();
    private boolean[][] inputArray = new boolean[][]
            {{false, false, false, false, false, false, true, false, false, false},
                    {false, false, false, false, true, false, false, false, true, false},
                    {false, false, false, false, true, false, false, false, false, false},
                    {false, true, false, true, false, false, false, false, false, false},
                    {true, true, false, false, false, false, false, false, false, false},
                    {true, false, false, false, false, false, false, false, true, false},
                    {false, false, false, false, false, false, true, false, true, false},
                    {false, false, false, true, false, false, false, false, false, false},
                    {false, false, false, true, false, false, true, true, false, true},
                    {false, true, false, false, false, true, false, false, false, true}};
    private int[][] result = new int[][]{{0, 0, 0, 1, 1, 2, -1, 2, 1, 1},
            {0, 0, 0, 2, -1, 3, 1, 2, -1, 1},
            {1, 1, 2, 3, -1, 2, 0, 1, 1, 1},
            {3, -1, 3, -1, 2, 1, 0, 0, 0, 0},
            {-1, -1, 3, 1, 1, 0, 0, 1, 1, 1},
            {-1, 3, 1, 0, 0, 1, 1, 3, -1, 2},
            {1, 1, 1, 1, 1, 1, -1, 3, -1, 2},
            {0, 0, 2, -1, 2, 2, 3, 4, 3, 2},
            {1, 1, 3, -1, 3, 2, -1, -1, 3, -1},
            {1, -1, 2, 1, 2, -1, 3, 2, 3, -1}};

    @Test
    void arrayWithNeighboring() {
        int[][] actual = minesweeper.arrayWithNeighboring(inputArray);
        assertEquals(Arrays.deepToString(result), Arrays.deepToString(actual));
    }
}