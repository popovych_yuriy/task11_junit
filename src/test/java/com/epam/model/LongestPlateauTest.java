package com.epam.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LongestPlateauTest {
    private LongestPlateau plateau = new LongestPlateau();
    private int[] input = new int[]{2, 3, 1, 1, 3, 2, 2, 4, 1, 2, 1, 3, 3, 4, 4, 4, 2, 1, 3, 2};
    private int resultIndex = 13;
    private int resultMax = 3;

    @Test
    void getStartIndex() {
        plateau.findPlateau(input);
        int actual = plateau.getStartIndex();
        assertEquals(resultIndex, actual);
    }

    @Test
    void getMax() {
        plateau.findPlateau(input);
        int actual = plateau.getMax();
        assertEquals(resultMax, actual);
    }
}