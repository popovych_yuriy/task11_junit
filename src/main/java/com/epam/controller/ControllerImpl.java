package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public void setPlateau() {
        model.setPlateau();
    }

    @Override
    public int getStartIndex() {
        return model.getStartIndex();
    }

    @Override
    public int getMax() {
        return model.getMax();
    }

    @Override
    public void setMinesweeper(int M, int N, float p) {
        model.setMinesweeper(M, N, p);
    }
}