package com.epam.controller;

public interface Controller {
    void setPlateau();

    int getStartIndex();

    int getMax();

    void setMinesweeper(int M, int N, float p);
}
