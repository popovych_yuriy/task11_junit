package com.epam.model;

public interface Model {
    void setPlateau();

    int getStartIndex();

    int getMax();

    void setMinesweeper(int M, int N, float p);
}
