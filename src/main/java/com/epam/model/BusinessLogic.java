package com.epam.model;

public class BusinessLogic implements Model {
    LongestPlateau plateau;
    Minesweeper minesweeper;

    @Override
    public void setPlateau() {
        plateau = new LongestPlateau();
    }

    @Override
    public int getStartIndex() {
        return plateau.getStartIndex();
    }

    @Override
    public int getMax() {
        return plateau.getMax();
    }

    @Override
    public void setMinesweeper(int M, int N, float p) {
        minesweeper = new Minesweeper();
        boolean[][] mines;
        mines = minesweeper.createBombsArray(M, N, p);
        minesweeper.printBombs(mines);
        minesweeper.printWithNumbers(minesweeper.arrayWithNeighboring(mines));
    }
}
