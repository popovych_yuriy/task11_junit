package com.epam.model;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Minesweeper {
    private boolean [][] bombs;

    public boolean [][] createBombsArray(int height, int length, float probability){
        boolean [][] bombs = new boolean[height][length];
        for (int i = 0; i < bombs.length; i++) {
            for (int j = 0; j < bombs[i].length; j++) {
                bombs[i][j] = Math.random() < probability;
            }
        }
        System.out.println(Arrays.deepToString(bombs));
        return bombs;
    }

    public void printBombs(boolean [][] bombs){
        for (boolean[] row : bombs) {
            for (boolean cell : row) {
                if(cell){
                    System.out.print("*");
                }else {
                    System.out.print(".");
                }
                System.out.print("\t");
            }
            System.out.println();
        }
    }

    public int [][] arrayWithNeighboring(boolean[][] bombs){
        int length = bombs.length;
        int height = bombs[0].length;
        int [][] result = new int[height][length];

        for ( int i = 0; i < height; ++i ) {
            for ( int j = 0; j < length; ++j ) {
                if (bombs[i][j]) {
                    result[i][j] = -1;
                }else{
                    result[i][j] = calculateBombs(bombs, i, j);
                }
            }
        }
        return result;
    }

    private int calculateBombs(boolean[][] bombs, int i, int j) {
        int counter = 0;
        if(checkIndex(bombs, i-1, j-1) && bombs[i-1][j-1]){
            counter++;
        }
        if(checkIndex(bombs, i-1, j) && bombs[i-1][j]){
            counter++;
        }
        if(checkIndex(bombs, i-1, j+1) && bombs[i-1][j+1]){
            counter++;
        }
        if(checkIndex(bombs, i, j-1) && bombs[i][j-1]){
            counter++;
        }
        if(checkIndex(bombs, i, j+1) && bombs[i][j+1]){
            counter++;
        }
        if(checkIndex(bombs, i+1, j-1) && bombs[i+1][j-1]){
            counter++;
        }
        if(checkIndex(bombs, i+1, j) && bombs[i+1][j]){
            counter++;
        }
        if(checkIndex(bombs, i+1, j+1) && bombs[i+1][j+1]){
            counter++;
        }
        return counter;
    }

    private boolean checkIndex(boolean[][] bombs, int i, int j){
        int height = bombs.length;
        int length = bombs[0].length;
        return (i >= 0 && j >= 0 && i < length && j < length && i < height && j < height);
    }

    public void printWithNumbers(int [][] bombs){
        for (int[] row : bombs) {
            for (int cell : row) {
                if(cell == -1){
                    System.out.print("*");
                }else{
                    System.out.print(cell);
                }
                System.out.print("\t");
            }
            System.out.println();
        }
    }
}
