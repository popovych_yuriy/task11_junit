package com.epam.model;

public class LongestPlateau {
    private int startIndex;
    private int max;

    public LongestPlateau() {
        findPlateau(generateNumbers());
    }

    private int[] generateNumbers() {
        int[] numbers = new int[20];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (int) (Math.random() * 4 + 1);
        }
        return numbers;
    }

    public void findPlateau(int[] numbers) {
        int count = 1;
        max = 0;
        startIndex = 0;

        for (int i = 2; i < numbers.length; i++) {
            if ((numbers[i - 1] == numbers[i]) && (numbers[i - 2] <= numbers[i - 1])) {
                count++;
            } else {
                count = 1;
            }

            if (count > max && i + 1 < numbers.length && numbers[i] > numbers[i + 1]) {
                max = count;
                startIndex = i - max + 1;
            }
        }
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getMax() {
        return max;
    }
}
